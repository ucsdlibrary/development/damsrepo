package edu.ucsd.library.dams.unitTest.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.XPath;
import org.dom4j.io.SAXReader;
import org.junit.Before;
import org.junit.Test;

import edu.ucsd.library.dams.api.DAMSAPIServlet;

public class DAMSAPIServletTest extends UnitTestBasic {
    private static final String DATA_CITE_XSL = "datacite.xsl";
    private static final String DAMS_RDF_SOURCE = "damsObjectEzid.rdf.xml";
    private static final String DAMS_RDF_SOURCE_COLLECTION_DOI = "damsCollectionDOI.rdf.xml";
    private static final String DAMS_RDF_SOURCE_SAME_TITLE = "damsObjectEzidSameTitle.rdf.xml";
    private static final String TEST_URI = "http://library.ucsd.edu/dc/object/bb9182008x";

    private Map<String, String> namespaces = null;

    private File rdfFile = null;
    private File rdfWithCollectionDoi = null;
    private File rdfFileSameTitle = null;
    private Transformer transformer = null;
    @Before
    public void init() throws DocumentException, IOException, TransformerConfigurationException {
        File xsl = getResourceFile(DATA_CITE_XSL);
        rdfFile = getResourceFile(DAMS_RDF_SOURCE);
        rdfWithCollectionDoi = getResourceFile(DAMS_RDF_SOURCE_COLLECTION_DOI);
        rdfFileSameTitle = getResourceFile(DAMS_RDF_SOURCE_SAME_TITLE);
        TransformerFactory tf = TransformerFactory.newInstance();
        transformer = tf.newTransformer( new StreamSource(xsl) );
        namespaces = new HashMap<String, String>();
        namespaces.put("dc", "http://datacite.org/schema/kernel-4");
    }

    @Test
    public void testDataCiteTransform() throws Exception {
        SAXReader saxReader = new SAXReader();
        Document rdf = saxReader.read(rdfFile);
        String dataCiteXml = DAMSAPIServlet.xslt(rdf.asXML(), transformer, new HashMap<String, String[]>(), "");

        try (InputStream in = new ByteArrayInputStream(dataCiteXml.getBytes("utf-8"))) {
            Document dataCiteDoc = saxReader.read(in);
            assertEquals("Size of relatedIdentifiers  doesn't match!", 3,
                    createXPath("/dc:resource/dc:relatedIdentifiers/dc:relatedIdentifier").selectNodes(dataCiteDoc).size());

            assertEquals("RelatedIdentifier IsSupplementTo doesn't match!", TEST_URI,
                    createXPath("/dc:resource/dc:relatedIdentifiers/dc:relatedIdentifier[@relationType='IsSupplementTo' and @relatedIdentifierType='URL']").valueOf(dataCiteDoc));
            assertEquals("RelatedIdentifier References doesn't match!", "https://arxiv.org/anyid",
                    createXPath("/dc:resource/dc:relatedIdentifiers/dc:relatedIdentifier[@relationType='References' and @relatedIdentifierType='arXiv']").valueOf(dataCiteDoc));
            assertNull("RelatedIdentifier 'OnlineFindingAid' should not exist!",
                    createXPath("/dc:resource/dc:relatedIdentifiers/dc:relatedIdentifier[@relationType='OnlineFindingAid']").selectSingleNode(dataCiteDoc));

            // Validate RelatedIdentifier IsPartOf for collection DOI
            assertEquals("RelatedIdentifier IsPartOf for collection DOI doesn't match!", "https://doi.org/10.6075/anyid",
                    createXPath("/dc:resource/dc:relatedIdentifiers/dc:relatedIdentifier[@relationType='IsPartOf' and @relatedIdentifierType='DOI']").valueOf(dataCiteDoc));

            // Validate Note[dams:type='technical details'] =>  Description:TechnicalInfo
            assertEquals("Description:Technical Info doesn't match!", "Technical Details Note",
                    createXPath("/dc:resource/dc:descriptions/dc:description[@descriptionType='TechnicalInfo']").valueOf(dataCiteDoc));

            // Validate subject dams:Anatomy
            assertEquals("Subject dams:Anatomy doesn't match!", "Test Anatomy",
                    createXPath("/dc:resource/dc:subjects/dc:subject").valueOf(dataCiteDoc));
        }
    }

    @Test
    public void testDataCiteCollectionDoiTransform() throws Exception {
        SAXReader saxReader = new SAXReader();
        Document rdf = saxReader.read(rdfWithCollectionDoi);
        String dataCiteXml = DAMSAPIServlet.xslt(rdf.asXML(), transformer, new HashMap<String, String[]>(), "");

        try (InputStream in = new ByteArrayInputStream(dataCiteXml.getBytes("utf-8"))) {
            Document dataCiteDoc = saxReader.read(in);
            // Validate RelatedIdentifier IsPartOf for collection DOI
            assertEquals("RelatedIdentifier IsPartOf for collection DOI doesn't match!", "https://doi.org/10.6075/anyid",
                    createXPath("/dc:resource/dc:relatedIdentifiers/dc:relatedIdentifier[@relationType='IsPartOf' and @relatedIdentifierType='DOI']").valueOf(dataCiteDoc));
        }
    }

    @Test
    public void testDataCiteSameTitleTransform() throws Exception {
        SAXReader saxReader = new SAXReader();
        Document rdf = saxReader.read(rdfFileSameTitle);
        String dataCiteXml = DAMSAPIServlet.xslt(rdf.asXML(), transformer, new HashMap<String, String[]>(), "");

        try (InputStream in = new ByteArrayInputStream(dataCiteXml.getBytes("utf-8"))) {
            Document dataCiteDoc = saxReader.read(in);
            assertEquals("Size of title  doesn't match!", 1,
                    createXPath("/dc:resource/dc:titles/dc:title").selectNodes(dataCiteDoc).size());

            assertEquals("Title doesn't match!", "Test Related Resource - Same Title",
                    createXPath("/dc:resource/dc:titles/dc:title").valueOf(dataCiteDoc));

            // Validate no RelatedIdentifier IsPartOf for object with the same DOI as collection
            String xpath = "/dc:resource/dc:relatedIdentifiers/dc:relatedIdentifier[@relationType='IsPartOf']";
            assertNull("RelatedIdentifier IsPartOf for collection DOI found!",
                    createXPath(xpath).selectSingleNode(dataCiteDoc));
        }
    }

    /*
     * Create XPath for MARC/MODS elements selection
     * @param xpathExpression
     * @return
     */
    private XPath createXPath(String xpathExpression) {
        XPath xPath = DocumentHelper.createXPath(xpathExpression);
        xPath.setNamespaceURIs(namespaces);
        return xPath;
    }

}
