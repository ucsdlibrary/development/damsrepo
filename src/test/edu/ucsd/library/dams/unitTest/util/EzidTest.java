package edu.ucsd.library.dams.unitTest.util;

import static org.junit.Assert.assertTrue;
import org.junit.Test;

import edu.ucsd.library.dams.util.Ezid;
import edu.ucsd.library.dams.util.EzidException;
import edu.ucsd.library.dams.unitTest.api.UnitTestBasic;

/**
 * Test methods for Ezid class
 * @author lsitu
 *
 */
public class EzidTest extends UnitTestBasic {

    private static final String TEST_RDF_FILE_NAME = "damsObjectDoi.rdf.xml";

    @Test(expected = EzidException.class)
    public void testValidateDoi() throws Exception {
        String rdfXml = getResourceAsString(TEST_RDF_FILE_NAME);

        Ezid.validate(rdfXml);
    }
}
