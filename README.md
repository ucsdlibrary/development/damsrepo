The UC San Diego Library DAMS repository
========================================

DAMS Repository is a digital object repository developed at the
[UC San Diego Library](http://library.ucsd.edu/).
[Hydra](http://projecthydra.org/) frontends, such as
[DAMS PAS](http://github.com/ucsdlib/damspas)
are supported by a partial implementaiton of the
[Fedora 3.x REST API](https://wiki.duraspace.org/display/FEDORA38/REST+API).

[Setup Instructions](https://github.com/ucsdlib/damsrepo/wiki/Setup) are in the wiki.

## Load Testing
[Grafana k6][k6] is used for load testing scripts. See the `load/` directory for current scripts and usage instructions.

License
-------
* [FileStoreServlet.java](src/java/edu/ucsd/library/dams/api/FileStoreServlet.java) is licensed under the [GNU LGPL](http://www.gnu.org/licenses/lgpl.html).
* DAMS Repository code is governed by the [UC Copyright Notice](UC_Copyright_Notice.txt).
* See [third-party licenses](third-party-licenses.txt) for more information about licenses for
  Java libraries.

[k6]:https://grafana.com/docs/k6/latest/
