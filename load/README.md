# Load Testing

[Grafana k6][k6] is used for load testing damsrepo.

If running the script(s) locally, we recommend using the docker image as follows. Replace the script name with the
desired load testing script.

```sh
docker run --rm -i grafana/k6 run --verbose - <clientinfo.js
```


[k6]:https://grafana.com/docs/k6/latest/
