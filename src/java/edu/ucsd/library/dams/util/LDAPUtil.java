package edu.ucsd.library.dams.util;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.naming.NamingException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import edu.ucsd.library.utils.ADBridgeClient;

/**
 * Utility to lookup properties from LDAP.
 * @author escowles
**/
public class LDAPUtil
{
	private Properties props;
	private String ldapURL;
	private String ldapPrincipal;
	private String ldapPass;
	private String[] defaultAttributes;
	private ADBridgeClient adBridgeClient;

	public LDAPUtil( Properties props )
	{
		// ldap authorization
		this.props= props;

		ldapURL = props.getProperty( "ldap.url" );
		ldapPrincipal = props.getProperty( "ldap.principal" );
		ldapPass = props.getProperty( "ldap.pass" );

		String tmp = props.getProperty("ldap.defaultAttributes");
		if ( tmp != null )
		{
			defaultAttributes = tmp.split(",");
		}

		try {
			adBridgeClient = new ADBridgeClient(ldapURL);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Map<String,List<String>> lookup( String user, String[] attribs )
		throws NamingException
	{
		Map<String,List<String>> info = new HashMap<String,List<String>>();

		// check config and parameter
		if ( user == null || user.trim().equals("") || ldapURL == null
			|| ldapPrincipal == null )
		{
			return info;
		}

		if ( attribs == null ) { attribs = defaultAttributes; }

		try
		{
			// get properties
			String userInfoJson = adBridgeClient.retrieveUserInfo(user, ldapPrincipal, ldapPass);
			JSONObject results = (JSONObject)JSONValue.parse(userInfoJson);
			for ( int i = 0; i < attribs.length; i++ )
			{
				List<String> list = new ArrayList<String>();

				// ADBridge use key email instead of mail
				String attribName = attribs[i].equals("mail") ? "email" : attribs[i];
				Object attrValue = results.get( attribName );
				JSONArray values = new JSONArray();
				values.addAll(attrValue instanceof JSONArray ? (JSONArray)attrValue : Arrays.asList(attrValue));

				String m  = props.getProperty("ldap." + attribs[i] + ".match");
				String t1 = props.getProperty("ldap." + attribs[i] + ".trimstart");
				String t2 = props.getProperty("ldap." + attribs[i] + ".trimend");
				for ( int j = 0; values != null && j < values.size(); j++ )
				{
					String val = (String)values.get(j);
					if ( m == null || val.indexOf(m) != -1 )
					{
						if ( t1 != null && val.indexOf(t1) != -1 )
						{
							val = val.substring( val.indexOf(t1) + t1.length() );
						}
						if ( t2 != null && val.indexOf(t2) > 0 )
						{
							val = val.substring( 0, val.indexOf(t2) );
						}
						list.add(val);
					}
				}
				info.put( attribs[i], list );
			}
		}
		catch ( Exception prex ) {
			prex.printStackTrace();
		}
		return info;
	}
}
