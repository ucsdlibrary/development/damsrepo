package edu.ucsd.library.dams.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.dom4j.DocumentException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 * Utility class to lookup/aggregate resource types from SOLR.
**/
public class ResourceTypeUtil {
	private String solrUrlBase;

	public ResourceTypeUtil(String solrUrlBase)
	{
		this.solrUrlBase = solrUrlBase;
	}

	/**
	 * Aggregate resource types from the collection objects.
	 * @param collection
	 * @return
	 * @throws IOException
	 * @throws DocumentException 
	 */
	public List<String> getResourceType(String collection) throws IOException {
		List<String> resourceTypes = new ArrayList<>();

		String solrUrl = solrUrlBase + "/select?" + getFacetObjectTypeQuery(collection, true);

		JSONObject result = (JSONObject) JSONValue.parse(HttpUtil.get(solrUrl));
		JSONObject facteFields = (JSONObject)((JSONObject)result.get("facet_counts")).get("facet_fields");
		JSONArray facetObjectTypes = (JSONArray)facteFields.get("object_type_sim");
		for (int i = 0; i < facetObjectTypes.size(); i++) {
			if (i % 2 == 0) {
				resourceTypes.add((String)facetObjectTypes.get(i));
			}
		}

		return resourceTypes;
	}

	/*
	 * build the query to lookup resource types from SOLR
	 * @param collection
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	private String getFacetObjectTypeQuery(String collection, boolean publicAccess) throws UnsupportedEncodingException {
		Map<String, String> params = new HashMap<>();
		String colArk = collection.indexOf("/") >= 0 ? collection.substring(collection.lastIndexOf("/") + 1) : collection;
		params.put("q", "collections_tesim:" + colArk);
		params.put("fq", "active_fedora_model_ssi:DamsObject");
		if (publicAccess) {
			params.put("fq", "discover_access_group_ssim:public");
		}
		// Aggregate resource types with faceting
		params.put("facet.field", "object_type_sim");
		params.put("wt", "json");
		params.put("rows", "" + 0);

		return buildSolrQueryString(params);
	}

	/**
	 * Build solr query from the parameters map
	 * @param params
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String buildSolrQueryString(Map<String, String> params) throws UnsupportedEncodingException {
		StringBuffer buf = new StringBuffer();
		for ( Iterator<String> it = params.keySet().iterator(); it.hasNext(); ) {
			String key = it.next();
			String val = params.get(key);
			if (StringUtils.isNotBlank(val)) {
				if (buf.length() > 0) {
					buf.append("&");
				}
				buf.append(URLEncoder.encode(key, "UTF-8"));
				buf.append("=");
				buf.append(URLEncoder.encode(val, "UTF-8"));
			}
		}
		return buf.toString();
	}
}
