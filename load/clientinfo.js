import http from 'k6/http';
import { check, sleep } from 'k6';

export const options = {
  // damsrepo won't verify
  insecureSkipTLSVerify: true,
  vus: 10,
  duration: '30s',
};

export default function () {
  // other enviroments to test
  // const url = "http://lib-hydratail-staging.ucsd.edu:8080/dams/api/client/info?user=mcritchlow&format=json"
  // const url = "https://lib-hydratail-prod.ucsd.edu:8443/dams/api/client/info?user=mcritchlow&format=json"
  //
  // change username as needed/desired
  const url = "http://localhost:8080/dams/api/client/info?user=mcritchlow&format=json"
  const response = http.get(url);

  check(response, { 'status is 200': (r) => r.status === 200 });
  sleep(1)
}

